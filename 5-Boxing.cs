namespace Boxing;

public static class Explain
{
    public static void TwoNamesOneType()
    {
        int a = 0;
        Int32 b = 0;

        _ = a.Equals(b);
        _ = b.Equals(a);

        bool c = false;
        Boolean d = false;

        float e = 0.0f;
        Single f = 0.0f;

        double g = 0.0;
        Double h = 0.0;

        decimal i = 0m;
        Decimal j = 0m;

        string k = string.Empty;
        String l = String.Empty;
    }

    public struct ValueType
        : IEquatable<ValueType>
    {
        public bool Equals(ValueType other) => true;
    }

    public static void CheckViaVirtualDispatch<T>(IEquatable<T> boxed)
    {
    }

    public static void CheckViaReification<T>(T unboxed)
        where T: IEquatable<T>
    {
    }
}
