namespace ExtensionMethods;

public record class ReferenceType(int Value = 0);

public readonly record struct ValueType(int Value = 0);

public static class Extensions
{
    public static int AddToValue(this ReferenceType rt, int addend) =>
        rt.Value + addend;
    public static int AddToValue(this ValueType vt, int addend) =>
        vt.Value + addend;
}

public static class Explain
{
    public static void CallTwoWays()
    {
        var referenceValue = new ReferenceType();
        _ = referenceValue.AddToValue(3); // 3

        var valueValue = new ValueType(99);
        _ = valueValue.AddToValue(1); // 100

        var anotherReferenceValue = new ReferenceType(5);
        _ = Extensions.AddToValue(anotherReferenceValue, 6); // 11
    }
}
