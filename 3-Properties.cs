namespace Properties;

public class Point
{
    public int X { get; set; }
    public int Y { get; set; }
}

public class Point2
{
    public Point2(int x, int y)
    {
        X = x;
        Y = y;
    }

    public int X { get; }
    public int Y { get; }
}

public class Point3
{
    public int X { get; init; }
    public int Y { get; init; }
}

public class MisbehavingPoint
{
    int _x = 0;
    int _y = 0;

    public int X
    {
        get => _x;
        set => _x = value + 1;
    }

    public int Y
    {
        get => _y;
        set => _y = value - 1;
    }
}

public static class Explain
{
    public static void Initialize()
    {
        var p1 = new Point();
        p1.X = 44;
        p1.Y = 99;

        p1.X = 33;

        var p2 = new Point2(44, 99);

        // p2.X = 33;
        // Property or indexer 'Point2.X' cannot be assigned to -- it is read only

        var p3 = new Point3
        {
            X = 44,
            Y = 99,
        };

        // p3.X = 33;
        // Init-only property or indexer 'Point3.X' can only be assigned in an object initializer, or on 'this' or 'base' in an instance constructor or an 'init' accessor.
    }
}

public readonly record struct Point4(int X, int Y);
