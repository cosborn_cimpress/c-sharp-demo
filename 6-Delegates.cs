namespace Delegates;

public static class Explain
{
    public static void Lambda()
    {
        var aa = new[] { 1, 2, 3, 4, 5 };

        var bb = aa.Select(a => a + 1);
        // 2, 3, 4, 5, 6

        Func<int, int> increment1 = a => a + 1;
        var increment2 = (int a) => a + 1;
    }
}
