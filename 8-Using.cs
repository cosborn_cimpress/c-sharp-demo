namespace Using;

public static class Explain
{
    public static void ExplicitlyScopedAutoclose()
    {
        long length;
        using (var file = File.Open("/Users/cosborn/Developer/bastion.sh", FileMode.Open, FileAccess.Read))
        {
            length = file.Length;
        } // here

        Console.WriteLine(length);
    }

    public static void ImplicitlyScopedAutoclose()
    {
        using var file = File.Open("/Users/cosborn/Developer/bastion.sh", FileMode.Open, FileAccess.Read);
        var length = file.Length;
        Console.WriteLine(length);
    } // here

    public static void Expanded()
    {
        var file = File.Open("/Users/cosborn/Developer/bastion.sh", FileMode.Open, FileAccess.Read);
        try
        {
            _ = file.Length;
            // ImplicitlyScopedAutoclose writes here.
        }
        finally
        {
            if (file != null)
            {
                file.Dispose();
            }
        }

        // ExplicitlyScopedAutoclose writes here.
    }
}
