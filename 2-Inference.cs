namespace Inference;

public static class Explain
{
    public static void Var()
    {
        var a = 0;

        var b = true;

        // var c = null;
        // Cannot assign <null> to an implicitly-typed variable
    }

    public static void InferredConstructor()
    {
        List<int> aa = new();

        // List<int> bb = new List<>();
        // Unexpected use of an unbound generic name
    }

    static readonly List<int> s_integers = new();
    
    // static readonly var s_varIntegers = new List<int>();
    // The contextual keyword 'var' may only appear within a local variable declaration or in script code
}
