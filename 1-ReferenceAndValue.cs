namespace ReferenceAndValue;

public class ReferenceType
{
} 

public struct ValueType
{
}

public static class Explain
{
    public static void Nullable()
    {
        // ReferenceType attemptedDefaultReference = null;
        // Converting null literal or possible null value to non-nullable type.

        ReferenceType alsoDefaultReference = null!;
        ReferenceType? defaultNullableReference = null;

        // ValueType alsoDefaultValue = null!;
        // Cannot convert null to 'ValueType' because it is a non-nullable value type
        
        ValueType? defaultNullableValue = null;
        Nullable<ValueType> longFormDefaultNullableValue = null;

        // Nullable<ReferenceType> longFormDefaultNullableReference = null;
        // The type 'ReferenceType' must be a non-nullable value type in order to use it as parameter 'T' in the generic type or method 'Nullable<T>'
        //                                                 ----------
    }

    public static void Default()
    {
        ValueType defaultValue = default(ValueType);
        ReferenceType? defaultReference = default(ReferenceType);

        _ = default(bool) == false;
        _ = default(int) == 0;
        _ = default(float) == 0.0f;
        _ = default(string?) == null;
    }
}

public struct InheritedValueType
    // : ValueType
    // Type 'ValueType' in interface list is not an interface [demo]
{
}
