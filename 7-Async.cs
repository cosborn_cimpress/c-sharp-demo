namespace Async;

public static class Explain
{
    public static async Task<int> AwaitAsync()
    {
        await Task.Delay(TimeSpan.FromSeconds(5));
        Console.WriteLine("Hey, it's me!!!!");
        return 23;
    }

    public static async void AvoidThis()
    {
        await Task.Delay(TimeSpan.FromSeconds(5));
        throw new Exception("This will (almost) never be caught.");
    }
}
