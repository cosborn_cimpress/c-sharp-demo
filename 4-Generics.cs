using System.Numerics;

namespace Generics;

public class ReferenceType
{
}

public struct ValueType
{
}

public struct BiggerValueType
{
    public int TakesUpSpace { get; set; }
}

public static class Explain
{
    public static void NotBoxed()
    {
        var a = new List<ReferenceType>()
        {
            new ReferenceType(),
            new ReferenceType(),
        };

        var b = new List<ValueType>()
        {
            new ValueType(),
            new ValueType(),
        };
    }

    public static void Reified<T>(T value)
    {
        _ = value;
    }

    public static void CallReified()
    {
        Reified<ReferenceType>(new());
        Reified<int>(12);
        Reified<long>(default);
    }
}
